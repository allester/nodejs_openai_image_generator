const express = require('express')
const dotenv = require('dotenv')
const path = require('path')

dotenv.config()

// Import routes file
const openAIRoutes = require('./routes/openAIRoutes')

const port = process.env.PORT || 5000

const app = express()

// Enable body parser
app.use(express.json())
app.use(express.urlencoded({
    extended: false
}))

// Set static folder
app.use(express.static(path.join(__dirname, 'public')))

app.use('/openai', openAIRoutes)

app.listen(port, () => {
    console.log(`Server started on port ${port}`)
})