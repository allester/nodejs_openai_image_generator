# OpenAI Image Generator

This is a simple image generator built with Node.js and Express that uses OpenAI's Dall-E models to generate images.

![Image Generator Sample Image](https://res.cloudinary.com/dk8qdxlvl/image/upload/v1672320654/Git%20Projects/sample_cfqdjs.png)

## Usage

Rename the `example.env` file to `.env`.

Generate an API KEY at [OpenAI](https://beta.openai.com/) and add it to the `.env` file.

Install the dependencies

`npm install`

Run server

`npm start`

Visit `http://localhost:5000` in your browser.

The endpoint is at POST `http://localhost:5000/openai/generateimage`.